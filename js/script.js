var gameBlock = document.querySelectorAll('.game-block'),

    controlButtons = document.querySelector('.control-buttons'),

    successSound = document.getElementById('success'),
    failSound = document.getElementById('fail'),

    
    GamerName = document.querySelector('.info-containers .name span');

let duration = 1000;

let blocksContainer = document.querySelector('.memry-game-container');

let blocks = Array.from(blocksContainer.children);

let orderRange = [...Array(blocks.length).keys()];

let triesElement = document.querySelector('.tries span');

let FinishedGame = document.querySelector('.finish');

let playingScoreGamer = document.querySelector('.score-counter span');

//////////////////////////////////////////////
//############### Counter ###############

var seconds = 180; //number of seconds
var secondPass;
var container = document.querySelector('.counter span');
//////////////////////////////////////////////
//############### Counter ###############
//console.log(orderRange);
shuffle(orderRange);
//console.log(orderRange);
//console.log(orderRange);

///End Variables
controlButtons.onclick = function(){

    let yourName = prompt("Whates Your Name");

    //console.log(yourName);

    if(yourName == null || yourName == ""){
        GamerName.textContent = "User X";
    }else{
        GamerName.textContent = yourName;
    }
    controlButtons.remove();
    let countdown = setInterval(function(){
        "use strict";
        secondPass();
    },1000);
    

//////////////////////////////////////////////
//############### Counter ###############



//////////////////////////////////////////////
//############### Counter ###############

}
function secondPass(){
    "use strict";
    var minutes = Math.floor(seconds / 60);
    var remSeconds = seconds % 60;

    if(seconds < 100){
        remSeconds = "0" + remSeconds;
    }else if(seconds < 10){
        remSeconds = "0" + remSeconds;
    }

    container.innerHTML = minutes + ":" + remSeconds;

    if(seconds > 0){
        seconds = seconds - 1;//decrease one second every 1000s 
    }else{
        clearInterval(countdown);
        window.location.reload(true);
        container.innerHTML = "Done";
        container.style.animation = 'none';
    }
}

//Add Order Css property to Game Blocks
blocks.forEach((block,index) =>{
    //Add Css Order
    block.style.order = orderRange[index];

    //Add Click Event
    block.addEventListener('click',()=>{
        //Trigger the flip block function
        flipBlock(block)
    });

});

//Flip block function
function flipBlock(selectBlock){
    //Add Class Is Fliped
    selectBlock.classList.add('is-flipped');

    //Collect All Flipped cards
    let allFlipppedBlocks = blocks.filter(flippedBlock => flippedBlock.classList.contains('is-flipped'));

    //if theres tow selected blocks
    if(allFlipppedBlocks.length === 2){
        //console.log('tow blocks selected');

        //Stop clicking Function
        stopClicking();

        //Check Matched block function 
        chackMathchedBlocks(allFlipppedBlocks[0],allFlipppedBlocks[1]);

    }
}

//Stop clicking Function
function stopClicking(){

    //add class no clicking on main container
    blocksContainer.classList.add('no-clicking');

    setTimeout(()=>{
        //remove class no clicking After Duration
        blocksContainer.classList.remove('no-clicking');

    },duration);

};
//Check Matched block function 
function chackMathchedBlocks(firstBlock,secondBlock){
    //triesElement
    if(firstBlock.dataset.technology === secondBlock.dataset.technology){

        firstBlock.classList.remove('is-flipped');
        secondBlock.classList.remove('is-flipped');

        firstBlock.classList.add('has-matched');
        secondBlock.classList.add('has-matched');
        successSound.play();

        //Add To Score
        let finishingCounter = playingScoreGamer.innerHTML = parseInt(playingScoreGamer.innerHTML) + 20; 

        //If Play was Finished
        var x = document.querySelectorAll('.has-matched');
        if(x.length == orderRange.length){
            
            //Fetch .score and The Push Score
            document.querySelector('.finish .alert .score').textContent = finishingCounter;

            //Show Page Finish 
            FinishedGame.style.display = "block";

            //Stop Timer Count
            // clearInterval(secondPass());
            container.style.display = "none";

            let playAgain = document.querySelector('.play-again');
            playAgain.addEventListener("click",function(){
                
                window.location.reload(true);
            });

            let closeGame = document.querySelector('.exit-game');
            closeGame.addEventListener("click",function(){
                window.close();
            });
        }

    }else{
        triesElement.innerHTML = parseInt(triesElement.innerHTML) + 1;

        setTimeout(() => {

            firstBlock.classList.remove('is-flipped');
            secondBlock.classList.remove('is-flipped');
            failSound.play();
    
        },duration);
    
    }
}
//Shuffle Function
function shuffle(array){
    //Setting Var
    let current = array.length;
    let temp;
    let random;

    while(current > 0 ){
        //get Random

        random = Math.floor(Math.random() * current);

        //Decrease Length By One
        current --;

        //[1] save current Element In Stash
        temp = array[current];

        //[2] current Element = Randome Element 
        array[current] = array[random];

        //[3] Random Element = get Elemetnt From stach
        array[random] = temp;
        

    }
    return array;
}
